# conda-build-dind

A Dockerfile which adds "docker-in-docker" to the ics-docker/conda-build image

# To build the docker image:
`docker build . -t registry.esss.lu.se/johnsparger/conda-build-dind`

# To run the docker image:
Pass a command to run. The docker daemon will be started before you command is run.
For example, to attach to a bash shell in the conda-build-dind container:
`docker run --rm --privileged -it registry.esss.lu.se/johnsparger/conda-build-dind /bin/bash`

Or, to start an alpine container inside of the conda-build-dind container and attach to the alpine container's shell:
`docker run --rm --privileged -it registry.esss.lu.se/johnsparger/conda-build-dind docker run --rm -it alpine`

Example: start alpine using docker-in-docker and run apk list on the interactive shell
```
docker run --rm --privileged -it registry.esss.lu.se/johnsparger/conda-build-dind docker run --rm -it alpine
Unable to find image 'alpine:latest' locally
latest: Pulling from library/alpine
df20fa9351a1: Pull complete
Digest: sha256:185518070891758909c9f839cf4ca393ee977ac378609f700f60a771a2dfe321
Status: Downloaded newer image for alpine:latest
/ # apk list
WARNING: Ignoring APKINDEX.2c4ac24e.tar.gz: No such file or directory
WARNING: Ignoring APKINDEX.40a3604f.tar.gz: No such file or directory
musl-1.1.24-r8 x86_64 {musl} (MIT) [installed]
zlib-1.2.11-r3 x86_64 {zlib} (Zlib) [installed]
apk-tools-2.10.5-r1 x86_64 {apk-tools} (GPL-2.0-only) [installed]
musl-utils-1.1.24-r8 x86_64 {musl} (MIT BSD GPL2+) [installed]
libssl1.1-1.1.1g-r0 x86_64 {openssl} (OpenSSL) [installed]
alpine-baselayout-3.2.0-r6 x86_64 {alpine-baselayout} (GPL-2.0-only) [installed]
alpine-keys-2.2-r0 x86_64 {alpine-keys} (MIT) [installed]
busybox-1.31.1-r16 x86_64 {busybox} (GPL-2.0-only) [installed]
scanelf-1.2.6-r0 x86_64 {pax-utils} (GPL-2.0-only) [installed]
ca-certificates-bundle-20191127-r2 x86_64 {ca-certificates} (MPL-2.0 GPL-2.0-or-later) [installed]
libc-utils-0.7.2-r3 x86_64 {libc-dev} (BSD-2-Clause AND BSD-3-Clause) [installed]
libtls-standalone-2.9.1-r1 x86_64 {libtls-standalone} (ISC) [installed]
ssl_client-1.31.1-r16 x86_64 {busybox} (GPL-2.0-only) [installed]
libcrypto1.1-1.1.1g-r0 x86_64 {openssl} (OpenSSL) [installed]
```
