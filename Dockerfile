FROM registry.esss.lu.se/ics-docker/conda-build:latest

USER root

#### set up docker (from https://github.com/docker-library/docker/blob/master/19.03/Dockerfile)
ENV DOCKER_CHANNEL stable
ENV DOCKER_VERSION 18.09.9
ENV dockerArch x86_64

RUN if ! wget -O docker.tgz "https://download.docker.com/linux/static/${DOCKER_CHANNEL}/${dockerArch}/docker-${DOCKER_VERSION}.tgz"; then \
		echo >&2 "error: failed to download 'docker-${DOCKER_VERSION}' from '${DOCKER_CHANNEL}' for '${dockerArch}'"; \
		exit 1; \
	fi; \
	\
	tar --extract \
		--file docker.tgz \
		--strip-components 1 \
		--directory /usr/local/bin/ \
	; \
	rm docker.tgz; \
	\
	dockerd --version; \
	docker --version

#### set up dind (from https://github.com/docker-library/docker/blob/master/19.03/dind/Dockerfile)
# https://github.com/docker/docker/tree/master/hack/dind
RUN yum install -y iptables

ENV DIND_COMMIT 37498f009d8bf25fbb6199e8ccd34bed84f2874b

RUN set -eux; \
	wget -O /usr/local/bin/dind "https://raw.githubusercontent.com/docker/docker/${DIND_COMMIT}/hack/dind"; \
	chmod +x /usr/local/bin/dind

VOLUME /var/lib/docker
EXPOSE 2375 2376

#### install docker-compose
RUN pip install docker-compose

#### entrypoint
COPY entrypoint.sh /usr/local/bin/

ENTRYPOINT ["dind", "entrypoint.sh"]
