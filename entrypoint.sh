#!/bin/bash

# start docker daemon in the background
dockerd > /var/log/dockerd.log 2>&1 &

# wait for docker daemon to be ready
until docker info > /dev/null 2>&1
do
  sleep 1
done

# run the command
$@
